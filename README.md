Twitter timeline display plugin for [Grav CMS](http://getgrav.org)
-------------------------------------------------
This plugin dislay twitter timeline une a div.

## Working example

This `.md` page source:
```
---
title: home

twitty: true
twittyUsername: mytweetername
---
## My page

Bla bla bla and below my twitter :

TWITTYTEXT

blablabla


```
twitty: true => enalbe plugin for this page
twittyUsername: mytweetername => twitterusername
TWITTYTEXT will be replaced by my timeline, simple but working


## Installation

Git clone and put in `/your/site/grav/user/plugins/twitty' folder

## Configuration

All configuration rules located in `twitty.yaml`

You can just disable/enable plugin by changing `enabled` option, example:

enabled: true # enabled: false for disable

## License
The MIT License (MIT)

Copyright (c) 2014 Maxim Hodyrev

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
