<?php
namespace Grav\Plugin;

use Grav\Common\Page\Page;
use Grav\Common\Plugin;

class TwittyPlugin extends Plugin
{
	private $code = '<div class="container" align=center><a class="twitter-timeline" href="https://twitter.com/TWITTYNAME" data-widget-id="542690156990697473">Tweets de @TWITTYNAME</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?"http":"https";if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></div>';

    public static function getSubscribedEvents()
    {
    	return [
	      'onPluginsInitialized' => ['onPluginsInitialized', 0]
	    ];
    }



    //public function onPluginsInitialized(Event $event)
    public function onPluginsInitialized()
    {
	$this->enable([
	      'onPageInitialized'     => ['onPageInitialized', 0]
    	]);
    }

    public function onPageInitialized()
    {
	    $this->mergeConfig($this->grav['page']);
	    if ( $this->config->get('plugins.twitty.enabled') ) {
		      $page   = $this->grav['page'];
		      $twig   = $this->grav['twig'];
		      $uri    = $this->grav['uri'];
		      $options = $this->config->get('plugins.twitty');
		      $twittyUsername = $this->config->get('plugins.twitty.twittyUsername');
          	
		      //Read page and replace 
		      $page->content($this->processHtml($page,$twittyUsername));
	    }
    }
   
    private function processHtml($page,$twittyUsername)
    {
    	$content = $page->content();

	$replace = array('TWITTYTEXT'=>$this->code,'TWITTYNAME'=>$twittyUsername);
	return str_replace(array_keys($replace),array_values($replace), $content);

    }

  


   private function mergeConfig( Page $page )
   {
   	$defaults = (array) $this->config->get('plugins.twitty');
    	if ( isset($page->header()->twitty) ) {
      		$this->config->set('plugins.twitty.enabled', true);
    	} else {
      		$this->config->set('plugins.twitty.enabled', false);
    	}
   }

}
?>
